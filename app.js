var download = require( 'download' );
// Authenticate via OAuth
var tumblr = require( 'tumblr.js' ),
	_ = require( 'underscore' ),
	httpinvoke = require( 'httpinvoke' ),
	Q = require( 'q' ),
	http = require( 'http' ),
	Sync = require( 'sync' ),
	Url = require( 'url' ),
	Path = require( 'path' ),
	fs = require( 'fs' );
var tumblrClientConfig = JSON.parse( fs.readFileSync( __dirname + '/config.json' ) );
var client = tumblr.createClient( tumblrClientConfig );
console.log( 'authenticating with tumblr...' );
var totoalStored = 0;
var currentRequests = {};
var reqCount = 0;
var getLikes = function ( o ) {
	o = o || 0;
	var deferred = Q.defer();
	client.likes( {
		offset: o
	}, function ( err, d ) {
		if ( err ) deferred.reject( err );
		else {
			console.log( 'offset: ' + o + ' likes found: ' + d.liked_posts.length );
			deferred.resolve( d.liked_posts );
		}
	} );
	return deferred.promise;
};

function getter( url ) {
	if ( !currentRequests[ url.path ] ) currentRequests[ url.path ] = 0;
	currentRequests[ url.path ]++;
	if ( currentRequests[ url.path ] < 5 ) {
		if ( currentRequests[ url.path ] < 5 ) {
			currentRequests[ url.path ] = 5;
			var fname = url.pathname.substring( url.pathname.lastIndexOf( '/' ) + 1, url.pathname.length ).replace( '/', '_' );
			var fpath = Path.resolve( './cache/' + fname );
			console.log( 'requesting: "' + url.path + '", path: ' + fname );
			var file = fs.createWriteStream( fpath );
			reqCount++;
			Sync( function () {
				try {
					var dow = download( url.href, 'cache' );
					dow.on( 'error', function () {
						var dow = download( url.href, 'cache' );
						dow.on( 'error', function () {
							console.log( fname + ' failed!' );
							reqCount--;
						} );
					} );
				} catch ( e ) {
					console.error( e );
					reqCount++;
					var dow = download( url.href, 'cache' );
					dow.on( 'error', function () {
						console.log( fname + ' failed!' );
						reqCount--;
					} );
					dow.on( 'close', function () {
						reqCount--;
						console.log( 'done: "./cache/' + fname + '" currentRequests: ' + reqCount );
					} );
				}
			} );
			// httpinvoke( url.href, {
			// 	partialOutputMode: 'chunked'
			// } ).then( function ( res ) {
			// 	file.close();
			// 	reqCount--;
			// 	console.log( 'done: "./cache/' + fname + '" currentRequests: ' + reqCount + ' file size: ' + res.body.length );
			// 	currentRequests[ url.path ] = 5;
			// }, function ( err ) {
			// 	// 	console.log( 'Error occurred', err );
			// }, function ( progress ) {
			// 	if ( progress.type === 'download' ) {
			// 		file.write( progress.partial );
			// 	}
			// } );
		}
	}
}
//Makw the request
client.likes( function ( err, data ) {
	var totalLikes = data.liked_count;
	var offset = 7;
	var files = [];
	var current = 0;
	var limit = Math.floor( totalLikes / offset );
	var resultD = []; //	results cache
	var floorTotal = Math.floor( totalLikes );
	var links = []; //	link cache
	console.log( 'getting total of: ' + totalLikes + ' likes in: ' + limit + ' steps' );
	var images = [];
	var urlCache = [];
	var lgetters = [];
	for ( var i = limit; i >= 0; i-- ) {
		var currentOffset = offset * current;
		var currentStep = limit - i;
		var ldata = getLikes( currentOffset );
		current = current + 1;
		ldata.then( function ( d ) {
			_.each( d, function ( di ) {
				console.log( 'getting from: "' + di.blog_name + '" total versions for post: ' + di.photos.length );
				_.each( di.photos, function ( p ) {
					var _url = ( p.original_size.url ) ? p.original_size.url : p.alt_sizes[ 0 ].url;
					if ( urlCache.indexOf( _url ) <= -1 ) {
						console.log( 'added: "' + _url + '" ' + p.original_size.height + 'x' + p.original_size.width + ' queue size: ' + urlCache.length );
						urlCache.push( _url );
						var oU = Url.parse( _url );
						getter( oU );
					}
				} );
			} );
		} );
	}
} );